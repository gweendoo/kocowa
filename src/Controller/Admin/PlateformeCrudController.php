<?php

namespace App\Controller\Admin;

use App\Entity\Plateforme;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class PlateformeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Plateforme::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name_plateforme', 'Plateforme de visionnage'),
        ];
    }

    public function createEntity(string $entityFqcn)
    {
        // créer un objet vide
        $plateforme = new Plateforme();

        // on retourne la varibale est l'objet se créer tout seul
        return $plateforme;
    }
}
