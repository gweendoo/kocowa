<?php

namespace App\Controller\Admin;

use App\Entity\Acteur;
use App\Entity\Categories;
use App\Entity\Commentaires;
use App\Entity\Film;
use App\Entity\Plateforme;
use App\Entity\Role;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/kocowa09100', name: 'admin')]
    public function index(): Response
    {



        $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        return $this->redirect($adminUrlGenerator->setController(FilmCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
        // the name visible to end users
            ->setTitle('Kocowa')
        // the path defined in this method is passed to the Twig asset() function
            ->setFaviconPath('favicon.svg')
            ->setTranslationDomain('my-custom-domain')
            ->setTextDirection('ltr')
            ->renderContentMaximized()
            ->disableUrlSignatures()
            ->generateRelativeUrls();
    }

    public function configureMenuItems(): iterable
    {
        return[
            MenuItem::linkToRoute('Retour au site', 'fa fa-undo', 'home'),
            MenuItem::subMenu('Films', 'fas fa-film')->setSubItems([
                MenuItem::linkToCrud('tous les films', 'fas fa-film', Film::class),
                MenuItem::linkToCrud('Ajouter', 'fas fa-plus', Film::class)->setAction(Crud::PAGE_NEW),
                MenuItem::linkToCrud('Categories', 'fas fa-list', Categories::class),
                MenuItem::linkToCrud('Plateformes', 'fas fa-youtube', Plateforme::class),
            ]),
            MenuItem::subMenu('Acteurs', 'fas fa-user')->setSubItems([
                MenuItem::linkToCrud('Personnages', 'fas fa-user', Role::class),
                MenuItem::linkToCrud('Ajouter un personnages', 'fas fa-plus', Role::class)->setAction(Crud::PAGE_NEW),
            ]),
            MenuItem::linkToCrud('Commentaires', 'fas fa-comment', Commentaires::class),
            MenuItem::linkToCrud('Utilisateur', 'fas fa-user', User::class),
        ];

    }
}
