<?php

namespace App\Controller\Admin;

use App\Entity\Film;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class FilmCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Film::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('titre', 'Titre'),
            TextEditorField::new('description', 'Descripption'),
            TextField::new('annee', 'Année de sortie'),
            ChoiceField::new('note', 'Note sur 10')
                ->autocomplete()
                ->setChoices(['1'=>'1',
                    '2'=>'2',
                    '3'=>'3',
                    '4'=>'4',
                    '5'=>'5',
                    '6'=>'6',
                    '7'=>'7',
                    '8'=>'8',
                    '9'=>'9',
                    '10'=>'10',]),
            ChoiceField::new('statut', 'Statut')
                ->autocomplete()
                ->setChoices(['Vue'=>'Vue',
                    'En cours'=>'En cours',
                    'Pas encore vue' => 'Pas encore vue'
                    ]),
            AssociationField::new('categories'),
            AssociationField::new('roles', 'Nom des personnages'),
            AssociationField::new('plateformes'),
            TextField::new('video', 'Lien video'),
            TextField::new('image', 'Image du film'),
            TextField::new('banniere', 'Bannière du film'),


        ];
    }

    public function createEntity(string $entityFqcn)
    {
        // créer un objet vide
        $film = new Film();
        //ajout de l'utilisateur qui la créer
        $film ->setUtilisateur($this->getUser());
        // retourne la variable et le créer tout seul
        return $film;
    }

}
