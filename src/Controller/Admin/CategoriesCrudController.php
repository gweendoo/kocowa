<?php

namespace App\Controller\Admin;

use App\Entity\Categories;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CategoriesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Categories::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [

            TextField::new('label', 'Nom de la categories'),
        ];
    }

    public function createEntity(string $entityFqcn)
   {
       //créer un objet vide
       $categorie = new Categories();

       // return de la variable $categories et elle se créer toutes seul
        return $categorie;
    }

}
