<?php

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }


    public function configureFields(string $pageName): iterable
    {
        $password = TextField::new('password')
            ->setLabel("Password")
            ->setFormType(PasswordType::class)
            ->setFormTypeOption('empty_data', '')
            ->hideOnIndex(); // il ne sera visible que dans le formualire

        return [

            TextField::new('pseudo', 'Nom d\'utilisateur'),
            EmailField::new('Email', 'Adresse mail'),
            $password,
            ChoiceField::new('roles', 'Roles') // Définition du champ roles à multiples choix
            ->allowMultipleChoices()
                ->autocomplete()
                ->setChoices(['User' => 'ROLE_USER',
                        'Admin'=> 'ROLE_ADMIN']
                )
        ];
    }

}
