<?php

namespace App\Controller\Admin;

use App\Entity\Role;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class RoleCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Role::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name_role' , 'Nom du personnage'),
            TextField::new('name_acteur' , 'Nom de l\'acteur'),
            TextField::new('img_acteur' , 'Image de l\'acteur'),

        ];
    }
    public function createEntity(string $entityFqcn)
    {
        // créer un objet vide
        $role = new Role();
        // retourne sa variable est le créer tout seul
        return $role;
    }

}
