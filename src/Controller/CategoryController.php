<?php

namespace App\Controller;

use App\Entity\Categories;
use App\Entity\Film;
use App\Repository\CategoriesRepository;
use App\Repository\FilmRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    #[Route('/category/{slug}', name:'category_show')]
        public function show(?Categories $category):Response{


        return $this->render('category/show.html.twig',[
            'categories'=>$category,
        ]);
    }

}
