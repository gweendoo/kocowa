<?php

namespace App\Controller;

use App\Entity\Film;
use App\Repository\CategoriesRepository;
use App\Repository\FilmRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

    //Show all movies
    #[Route('/', name: 'home')]
    public function index(FilmRepository $filmRepository, PaginatorInterface $paginator , Request $request, CategoriesRepository $categoriesRepository): Response
    {
        $page = $paginator->paginate(
            $filmRepository->findAll(),
            $request->query->getInt('page', 1),
            12
        );
        return $this->render('home/index.html.twig', [
            'page' => $page,

            'categories' => $categoriesRepository->findAll(),
        ]);
    }
}
