<?php

namespace App\Controller;

use App\Form\SearchFilmType;
use App\Repository\FilmRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    #[Route('/FilmSearch', name: 'search')]
    public function searchBar()
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('handleSearch'))
            ->add('query', TextType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Entrez un mot-clé'
                ]
            ])
            ->add('recherche', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-primary'
                ]
            ])
            ->getForm();
        return $this->render('search/film.html.twig', [
            'formSearch' => $form->createView()
        ]);
    }
    #[Route('/handleSearch', name: 'handleSearch')]
    public function handleSearch(Request $request, FilmRepository $repo)
    {
        $query =   $request->request->all('form')['query'] ;
        if($query) {
            $films = $repo->findFilmByName($query);
        }
        return $this->render('search/index.html.twig', [
            'films' => $films
        ]);
    }



}
