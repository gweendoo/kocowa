<?php

namespace App\Controller;

use App\Entity\Commentaires;
use App\Form\CommentaireType;
use App\Form\EditProfilType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class CompteController extends AbstractController
{
    #[Route('/MonCompte', name: 'compte')]
    public function index(): Response
    {
        //Récupération de l'utilisateur connecter
        $user  = $this->getUser();
        $comment = $user->getCommentaires();
        return $this->render('compte/index.html.twig', [
            'user' => $user,
            'comment' => $comment,
        ]);
    }

    //edition du profil
    #[Route('/modif_profil', name:'editProfil')]
    public function editProfil(Request $request, ManagerRegistry $doctrine){
        $user=$this->getUser();
        $form=$this->createForm(EditProfilType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em=$doctrine->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('message', 'Profil mis a jour');
            return $this->redirectToRoute('compte');
        }
        return $this->render('compte/editProfil.html.twig',[
           'form'=>$form->createView(),
        ]);
    }
    //edition du mot de passe
    #[Route('/modif_passWord', name:'editPass')]
    public function editPasssWord(Request $request, ManagerRegistry $doctrine, UserPasswordHasherInterface $passwordEncoder){

        if($request->isMethod('POST')){
            $em=$doctrine->getManager();
            $user=$this->getUser();

            //Vérification de la corespondance des 2 mots de passe
            if($request->request->get('pass')==$request->request->get('pass2')){
                $user->setPassword($passwordEncoder->hashPassword($user,$request->get('pass')));
                $em->flush();
                $this->addFlash('message', 'Mot de passe mis à jour avec succès');
                return $this->redirectToRoute('compte');
            }else{
                $this->addFlash('erreur', 'Les deux mots de passe ne sont pas identiques');
            }
        }
        return $this->render('compte/editPassWord.html.twig');
    }

    #[Route('/editCommentaires/{id}', name: 'edit_commentaires')]
    public function editCommentaires(ManagerRegistry $doctrine, $id, Request $request){
        // récupération des informations du commentaires sélectionner par son id
        $commentaires=$doctrine->getRepository(Commentaires::class)->find($id);
        // création d'un nouveau formulaire
        $form = $this->createForm(CommentaireType::class, $commentaires);

        $form->handleRequest($request);
        // si le formulaire est valide
        if($form->isSubmitted() && $form->isValid()){
            // sauvgarde dans la base de données
            $em=$doctrine->getManager();
            $em->persist($commentaires);
            $em->flush();
            $this->addFlash('succes', 'votre commentaires à bien été modifier');

            // redirection vers la page compte de l'utilisateur
            return $this->redirectToRoute('compte');
        }
        return $this->render('compte/edit_comment.html.twig',[
            'form'=> $form->createView(),
        ]);
    }

    #[Route('/removeComment/{id}', name: 'remove_commentaires')]
    public function removeCommentaires(ManagerRegistry $doctrine, $id, Request $request){

        $remove = $doctrine->getRepository(Commentaires::class)->find($id);

        $em=$doctrine->getManager();
        $em ->remove($remove);
        $em->flush();
        $this->addFlash('succes', 'votre commentaires à bien été supprimer');

        return $this->redirectToRoute('compte');


    }
}
