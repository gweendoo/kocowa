<?php

namespace App\Controller;

use App\Entity\Commentaires;
use App\Entity\Film;
use App\Form\CommentaireType;
use App\Repository\CategoriesRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FilmController extends AbstractController
{
    #[Route('/film', name: 'film')]
    public function index(): Response
    {
        return $this->render('film/index.html.twig', [
            'controller_name' => 'FilmController',
        ]);
    }

    #[Route('film/{id}--{slug}', name: 'detail-film')]
    public function showDetailFilm(ManagerRegistry $doctrine, Request $request,  $id, CategoriesRepository $categoriesRepository):Response{
        //Récupération des données de filmRepository
        $repository=$doctrine->getRepository(Film::class);
        //on recupere l'id envoyer
        $film = $repository->find($id);

        $commentaires = new Commentaires();
        $form=$this->createForm(CommentaireType::class, $commentaires);
        $form->handleRequest($request);
        $user = $this->getUser();

        //envoye du formulaire
        if($form->isSubmitted() && $form->isValid()){
            $commentaires->setCreatedAt(new \DateTime());
            $commentaires->setUtilisateur($user);
            $commentaires->setFilms($film);
            //sauvgarde dans la base de donnée
            $em=$doctrine->getManager();
            $em->persist($commentaires);
            $em->flush();
            $this->addFlash('success', 'Votre commentaires est enregistré');
        }
        return $this->render('film/detail-film.html.twig',[
            'film' => $film,
            'form'=> $form->createView(),
            'categories' => $categoriesRepository->findAll(),

        ]);
    }
}
