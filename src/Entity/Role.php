<?php

namespace App\Entity;

use App\Repository\RoleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RoleRepository::class)]
class Role
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private ?string $name_role = null;


    #[ORM\ManyToMany(targetEntity: Film::class, mappedBy: 'roles')]
    private Collection $films;

    #[ORM\Column(length: 100)]
    private ?string $name_acteur = null;

    #[ORM\Column(length: 255)]
    private ?string $img_acteur = null;

    public function __construct()
    {
        $this->films = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameRole(): ?string
    {
        return $this->name_role;
    }

    public function setNameRole(string $name_role): self
    {
        $this->name_role = $name_role;

        return $this;
    }



    /**
     * @return Collection<int, Film>
     */
    public function getFilms(): Collection
    {
        return $this->films;
    }

    public function addFilm(Film $film): self
    {
        if (!$this->films->contains($film)) {
            $this->films->add($film);
        }

        return $this;
    }

    public function removeFilm(Film $film): self
    {
        $this->films->removeElement($film);

        return $this;
    }
    public function __toString():string{
        return $this ->name_role;
    }

    public function getNameActeur(): ?string
    {
        return $this->name_acteur;
    }

    public function setNameActeur(string $name_acteur): self
    {
        $this->name_acteur = $name_acteur;

        return $this;
    }

    public function getImgActeur(): ?string
    {
        return $this->img_acteur;
    }

    public function setImgActeur(string $img_acteur): self
    {
        $this->img_acteur = $img_acteur;

        return $this;
    }
}
